import models.Rectangle;
import models.Triangle;

public class App {
    public static void main(String[] args) throws Exception {
        Rectangle rectangle1 = new Rectangle(10,5,"Red" );
        Rectangle rectangle2 = new Rectangle(6,2,"Blue" );

        System.out.println(rectangle1.toString());
        System.out.println("Area: " + rectangle1.getArea());

        System.out.println(rectangle2.toString());
        System.out.println("Area: " + rectangle2.getArea());


        Triangle triangle1 = new Triangle(5, 10, "Yellow");
        Triangle triangle2 = new Triangle(7, 8, "Purple");

        System.out.println(triangle1.toString());
        System.out.println("Area: " + triangle1.getArea());

        System.out.println(triangle2.toString());
        System.out.println("Area: " + triangle2.getArea());
    }
}
